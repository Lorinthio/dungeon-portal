package me.lorinth.dungeonportal.boss;

import io.lumine.xikage.mythicmobs.MythicMobs;
import io.lumine.xikage.mythicmobs.adapters.AbstractLocation;
import io.lumine.xikage.mythicmobs.adapters.bukkit.BukkitAdapter;
import io.lumine.xikage.mythicmobs.mobs.MythicMob;
import me.lorinth.dungeonportal.util.DungeonPortalOutputHandler;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import me.lorinth.utils.objects.Version;

import java.lang.reflect.Method;

public class MythicMobsSpawnHandler implements IBossHandler {

    private Version mythicMobsVersion;
    private Method spawnMethod;

    public MythicMobsSpawnHandler(){
        mythicMobsVersion = new Version(MythicMobs.inst().getVersion());
    }

    public boolean spawnBoss(Location location, String name, Integer level) {
        if (Bukkit.getPluginManager().getPlugin("MythicMobs") == null) {
            return false;
        }

        MythicMob boss = MythicMobs.inst().getMobManager().getMythicMob(name);
        if (boss == null) {
            return false;
        }
        spawnBoss(boss, BukkitAdapter.adapt(location), level);
        return true;
    }

    private void spawnBoss(MythicMob boss, AbstractLocation location, int level) {
        if(this.mythicMobsVersion.getMajorVersion() > 4 ||
                (this.mythicMobsVersion.getMajorVersion() == 4 && this.mythicMobsVersion.getMinorVersion() >= 9)) {
            try {
                if(spawnMethod == null)
                    spawnMethod = boss.getClass().getMethod("spawn", AbstractLocation.class, double.class);

                spawnMethod.invoke(boss, location, (double) level);
            } catch(Exception ex) {
                DungeonPortalOutputHandler.PrintException("Unable to find method, 'setMobLevel' on 'MythicMobSpawnEvent' with parameter {level: double}", ex);
            }
        } else {
            try {
                if(spawnMethod == null)
                    spawnMethod = boss.getClass().getMethod("spawn", AbstractLocation.class, int.class);

                spawnMethod.invoke(boss, location, level);
            } catch(Exception ex) {
                DungeonPortalOutputHandler.PrintException("Unable to find method, 'setMobLevel' on 'MythicMobSpawnEvent' with parameter {level: int}", ex);
            }
        }
    }

}
