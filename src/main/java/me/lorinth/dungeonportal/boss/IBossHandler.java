package me.lorinth.dungeonportal.boss;

import org.bukkit.Location;

public interface IBossHandler {
    boolean spawnBoss(Location location, String name, Integer level);
}
