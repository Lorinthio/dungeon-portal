package me.lorinth.dungeonportal.boss;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.mineacademy.boss.api.Boss;
import org.mineacademy.boss.api.BossAPI;
import org.mineacademy.boss.api.BossSpawnReason;

public class BossSpawnHandler implements IBossHandler {

    public boolean spawnBoss(Location location, String name, Integer level){
        if(Bukkit.getPluginManager().getPlugin("Boss") == null){
            return false;
        }

        Boss boss = BossAPI.getBoss(name);
        if(boss == null){
            return false;
        }
        boss.spawn(location, BossSpawnReason.CUSTOM);
        return true;
    }

}
