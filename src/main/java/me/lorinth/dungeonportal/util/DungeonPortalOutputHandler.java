package me.lorinth.dungeonportal.util;

import me.lorinth.utils.OutputHandler;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

public class DungeonPortalOutputHandler extends OutputHandler {

    private static DungeonPortalOutputHandler instance;
    public static ChatColor INFO;
    public static ChatColor ERROR;
    public static ChatColor COMMAND;
    public static ChatColor HIGHLIGHT;

    public DungeonPortalOutputHandler() {
        super("DungeonPortal",
                ChatColor.DARK_PURPLE, //info
                ChatColor.RED,  //error
                ChatColor.LIGHT_PURPLE, //command
                ChatColor.GOLD);//highlight
        
        instance = this;
        INFO = getInfo();
        ERROR = getError();
        COMMAND = getCommand();
        HIGHLIGHT = getHighLight();
    }

    public static DungeonPortalOutputHandler getInstance(){
        return instance;
    }

    public static void PrintInfo(String message){
        instance.printInfo(message);
    }

    public static void PrintRawInfo(String message){
        instance.printRawInfo(message);
    }

    public static void PrintError(String message){
        instance.printError(message);
    }

    public static void PrintRawError(String message){
        instance.printRawError(message);
    }

    public static void PrintInfo(CommandSender sender, String message){
        instance.printInfo(sender, message);
    }

    public static void PrintCommand(CommandSender sender, String message){
        instance.printCommand(sender, message);
    }

    public static void PrintRawInfo(CommandSender sender, String message){
        instance.printRawInfo(sender, message);
    }

    public static void PrintError(CommandSender sender, String message){
        instance.printError(sender, message);
    }

    public static void PrintRawError(CommandSender sender, String message){
        instance.printRawError(sender, message);
    }

    public static void PrintException(String message, Exception exception){
        instance.printException(message, exception);
    }

    public static void PrintWhiteSpace(CommandSender sender, int lines){
        instance.printWhiteSpace(sender, lines);
    }
}
