package me.lorinth.dungeonportal.events;

import me.lorinth.dungeonportal.models.Portal;
import me.lorinth.dungeonportal.models.PortalInstance;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import java.util.List;

public class PortalTeleportEvent extends Event implements Cancellable {

    private static final HandlerList handlers = new HandlerList();
    private List<Player> players;
    private Portal portal;
    private PortalInstance portalInstance;
    private boolean isCancelled;

    public PortalTeleportEvent(List<Player> players, Portal portal, PortalInstance portalInstance){
        this.players = players;
        this.portal = portal;
        this.portalInstance = portalInstance;
    }

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }

    public List<Player> getPlayers(){
        return players;
    }

    public Portal getPortal(){
        return portal;
    }

    public PortalInstance getPortalInstance(){
        return portalInstance;
    }

    @Override
    public boolean isCancelled() {
        return isCancelled;
    }

    @Override
    public void setCancelled(boolean cancelled) {
        isCancelled = cancelled;
    }
}
