package me.lorinth.dungeonportal.messages;

import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;

public class MessageHandler {

    public static String PortalNotActive;
    public static String PortalAlreadyTaken;
    public static String PlayerNotLeader;

    private final String relativePath = "Messages.";

    public MessageHandler(FileConfiguration config){
        PortalNotActive = ChatColor.translateAlternateColorCodes('&', config.getString(relativePath + ".PortalNotActive"));
        PortalAlreadyTaken = ChatColor.translateAlternateColorCodes('&', config.getString(relativePath + ".PortalAlreadyTaken"));
        PlayerNotLeader = ChatColor.translateAlternateColorCodes('&', config.getString(relativePath + ".PlayerNotLeader"));
    }

}
