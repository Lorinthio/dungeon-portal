package me.lorinth.dungeonportal.commands;

import me.lorinth.dungeonportal.DungeonPortalPlugin;
import me.lorinth.dungeonportal.models.Portal;
import me.lorinth.dungeonportal.util.DungeonPortalOutputHandler;
import me.lorinth.utils.commands.ICommandExecutor;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

public class PortalListCommand implements ICommandExecutor {
    @Override
    public void execute(CommandSender commandSender, String[] args) {
        DungeonPortalOutputHandler.PrintRawInfo(commandSender,
                DungeonPortalOutputHandler.COMMAND + "" + ChatColor.BOLD + "Dungeon Portal List");
        for(Portal p : DungeonPortalPlugin.getPortalManager().getAllPortals()){
            DungeonPortalOutputHandler.PrintRawInfo(commandSender,
                    DungeonPortalOutputHandler.COMMAND + " - " + p.getId());
        }
    }

    @Override
    public void sendHelp(CommandSender commandSender) {

    }
}
