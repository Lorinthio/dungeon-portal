package me.lorinth.dungeonportal.commands;

import me.lorinth.dungeonportal.DungeonPortalPlugin;
import me.lorinth.dungeonportal.models.Portal;
import me.lorinth.dungeonportal.models.PortalInstance;
import me.lorinth.dungeonportal.util.DungeonPortalOutputHandler;
import me.lorinth.utils.commands.ICommandExecutor;
import org.bukkit.command.CommandSender;

public class PortalStartCommand implements ICommandExecutor {

    @Override
    public void execute(CommandSender commandSender, String[] args) {
        if(args.length == 0){
            sendHelp(commandSender);
            return;
        }

        String id = args[0];
        Portal p = DungeonPortalPlugin.getPortalManager().getPortalById(id);
        if(p == null){
            DungeonPortalOutputHandler.PrintError(commandSender, "Unable to find a portal with the id, " + DungeonPortalOutputHandler.HIGHLIGHT + id);
            return;
        }

        PortalInstance instance = p.activate();
        DungeonPortalOutputHandler.PrintRawInfo(commandSender, DungeonPortalOutputHandler.COMMAND + "Boss Id : " + instance.getBossResult().getBossId());
        DungeonPortalOutputHandler.PrintRawInfo(commandSender, DungeonPortalOutputHandler.COMMAND + "Boss Level : " + instance.getBossResult().getLevel().toString());
        DungeonPortalOutputHandler.PrintRawInfo(commandSender, DungeonPortalOutputHandler.COMMAND + "Boss Location : " + instance.getBossResult().getSpawnLocation().toString());
        DungeonPortalOutputHandler.PrintRawInfo(commandSender, DungeonPortalOutputHandler.COMMAND + "Start Location : " + instance.getStartLocation().toString());
    }

    @Override
    public void sendHelp(CommandSender commandSender) {
        DungeonPortalOutputHandler.PrintCommand(commandSender, "/portal start <id> - triggers a portal to open");
    }

}
