package me.lorinth.dungeonportal.commands;

import me.lorinth.dungeonportal.DungeonPortalPlugin;
import me.lorinth.dungeonportal.managers.PortalManager;
import me.lorinth.dungeonportal.models.Portal;
import me.lorinth.dungeonportal.util.DungeonPortalOutputHandler;
import me.lorinth.utils.commands.ICommandExecutor;
import org.bukkit.command.CommandSender;

public class PortalResetCommand implements ICommandExecutor {

    public void execute(CommandSender commandSender, String[] args){
        if(args.length == 0){
            sendHelp(commandSender);
            return;
        }

        String id = args[0];
        if(id.equalsIgnoreCase("all")){
            for(Portal p : DungeonPortalPlugin.getPortalManager().getAllPortals()){
                p.reset();
            }
            DungeonPortalOutputHandler.PrintInfo(commandSender,
                    "All portals have been reset.");
        } else {
            Portal p = DungeonPortalPlugin.getPortalManager().getPortalById(id);
            p.reset();

            DungeonPortalOutputHandler.PrintInfo(commandSender,
                    "The portal, "
                            + DungeonPortalOutputHandler.HIGHLIGHT + p.getId()
                            + DungeonPortalOutputHandler.INFO + " has been reset.");
        }
    }

    @Override
    public void sendHelp(CommandSender commandSender) {
        DungeonPortalOutputHandler.PrintCommand(commandSender, "/portal reset <id>");
        DungeonPortalOutputHandler.PrintCommand(commandSender, "/portal reset all");
    }

}
