package me.lorinth.dungeonportal.commands;

import me.lorinth.dungeonportal.DungeonPortalPlugin;
import me.lorinth.dungeonportal.util.DungeonPortalOutputHandler;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.jetbrains.annotations.NotNull;

import java.util.Arrays;

public class PortalCommand implements CommandExecutor {

    private PortalListCommand portalListCommand = new PortalListCommand();
    private PortalResetCommand portalResetCommand = new PortalResetCommand();
    private PortalStartCommand portalStartCommand = new PortalStartCommand();

    @Override
    public boolean onCommand(@NotNull CommandSender commandSender, @NotNull Command command, @NotNull String label, @NotNull String[] args) {
        if(args.length == 0){
            sendHelp(commandSender);
            return false;
        }

        String cmd = args[0];
        if(cmd.equalsIgnoreCase("list")) {
            portalListCommand.execute(commandSender, Arrays.copyOfRange(args, 1, args.length));
            return false;
        } else if(cmd.equalsIgnoreCase("reset")) {
            portalResetCommand.execute(commandSender, Arrays.copyOfRange(args, 1, args.length));
            return false;
        } else if(cmd.equalsIgnoreCase("start")) {
            portalStartCommand.execute(commandSender, Arrays.copyOfRange(args, 1, args.length));
            return false;
        }

        return false;
    }

    private void sendHelp(CommandSender commandSender){
        DungeonPortalOutputHandler.PrintCommand(commandSender, "/portal list - list all portal ids");
        DungeonPortalOutputHandler.PrintCommand(commandSender, "/portal reset <id> - reset a specific portal");
        DungeonPortalOutputHandler.PrintCommand(commandSender, "/portal reset all - reset all portals");
        DungeonPortalOutputHandler.PrintCommand(commandSender, "/portal start <id> - triggers a portal to open");
    }

}
