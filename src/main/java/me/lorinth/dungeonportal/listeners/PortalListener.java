package me.lorinth.dungeonportal.listeners;

import me.lorinth.dungeonportal.DungeonPortalPlugin;
import me.lorinth.dungeonportal.events.PortalEnterEvent;
import me.lorinth.dungeonportal.events.PortalTeleportEvent;
import me.lorinth.dungeonportal.messages.MessageHandler;
import me.lorinth.dungeonportal.models.Portal;
import me.lorinth.dungeonportal.models.PortalInstance;
import me.lorinth.dungeonportal.models.PossibleBossResult;
import net.Indyuce.mmocore.api.player.PlayerData;
import net.Indyuce.mmocore.api.player.social.Party;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class PortalListener implements Listener {

    @EventHandler(ignoreCancelled = true)
    public void validatePlayerEntry(PortalEnterEvent event){
        Player player = event.getPlayer();
        Portal portal = event.getPortal();
        if(!portal.isActive()){
            player.sendMessage(MessageHandler.PortalNotActive);
            event.setCancelled(true);
            return;
        }
        else if(!portal.isOpen()){
            player.sendMessage(MessageHandler.PortalAlreadyTaken);
            event.setCancelled(true);
            return;
        }
        else if(!isLeader(player)){
            player.sendMessage(MessageHandler.PlayerNotLeader);
            event.setCancelled(true);
            return;
        }

        PortalInstance instance = portal.start(player);
        PortalTeleportEvent portalTeleportEvent = new PortalTeleportEvent(getOnlinePartyMembers(player), portal, instance);
        Bukkit.getPluginManager().callEvent(portalTeleportEvent);

        if(!portalTeleportEvent.isCancelled()){
            //Teleport Players
            for(Player target : portalTeleportEvent.getPlayers()){
                target.teleport(instance.getStartLocation());
            }

            //Spawn Boss
            PossibleBossResult bossResult = instance.getBossResult();
            DungeonPortalPlugin.getBossHandler().spawnBoss(bossResult.getSpawnLocation(), bossResult.getBossId(), bossResult.getLevel());
        }
    }

    private boolean isLeader(Player player){
        UUID playerId = player.getUniqueId();
        PlayerData data = PlayerData.get(playerId);
        Party party = data.getParty();

        //If player not in party, they are solo and technically a leader
        if(party == null){
            return true;
        }

        // Check leader Id matches player Id
        UUID leaderId = party.getOwner().getUniqueId();
        return leaderId.toString().equalsIgnoreCase(playerId.toString());
    }

    private List<Player> getOnlinePartyMembers(Player player){
        UUID playerId = player.getUniqueId();
        PlayerData data = PlayerData.get(playerId);
        Party party = data.getParty();
        List<Player> players = new ArrayList<>();

        //If player not in party, they are solo and technically a leader
        if(party == null){
            players.add(player);
            return players;
        }

        party.getMembers().forEach((PlayerData targetPlayer) -> {
            if(targetPlayer.isOnline()){
                players.add(targetPlayer.getPlayer());
            }
        });
        return players;
    }

}
