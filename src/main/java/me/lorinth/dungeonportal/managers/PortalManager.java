package me.lorinth.dungeonportal.managers;

import me.lorinth.dungeonportal.models.Portal;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class PortalManager {

    private HashMap<String, Portal> portalsById;
    private List<Portal> allPortals;

    public PortalManager(){
        portalsById = new HashMap<>();
        allPortals = new ArrayList<>();
    }

    public void addPortal(Portal portal){
        portalsById.put(portal.getId(), portal);
        allPortals.add(portal);
    }

    public Portal getPortalById(String id){
        return portalsById.get(id);
    }

    public List<Portal> getAllPortals(){
        return allPortals;
    }

}
