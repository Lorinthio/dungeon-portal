package me.lorinth.dungeonportal.tasks;

import me.lorinth.dungeonportal.models.Portal;
import me.lorinth.dungeonportal.models.PortalTrigger;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.Random;

public class PortalActivationTask extends BukkitRunnable {

    private Portal portal;
    private Random random = new Random();

    public PortalActivationTask(Portal portal){
        this.portal = portal;
    }

    @Override
    public void run() {
        if(portal.isActive()){
            return;
        }

        PortalTrigger trigger = portal.getTrigger();
        if(random.nextDouble() * 100 < trigger.getChance()){
            portal.activate();
        }
        portal.updateNextTriggered();
    }

}
