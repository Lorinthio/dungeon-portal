package me.lorinth.dungeonportal.tasks;

import me.lorinth.dungeonportal.events.PortalEnterEvent;
import me.lorinth.dungeonportal.models.Portal;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.World;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.BoundingBox;

import java.util.Collection;

public class CheckPortalTask extends BukkitRunnable {

    private Portal portal;

    public CheckPortalTask(Portal portal){
        this.portal = portal;
    }

    @Override
    public void run() {
        if(!portal.getMinimum().getChunk().isLoaded() ||
            !portal.getMaximum().getChunk().isLoaded()){
            return;
        }

        Location min = portal.getMinimum();
        Location max = portal.getMaximum();

        World world = min.getWorld();
        int minX = Math.min(min.getBlockX(), max.getBlockX());
        int minY = Math.min(min.getBlockY(), max.getBlockY());
        int minZ = Math.min(min.getBlockZ(), max.getBlockZ());
        int maxX = Math.max(min.getBlockX(), max.getBlockX());
        int maxY = Math.max(min.getBlockY(), max.getBlockY());
        int maxZ = Math.max(min.getBlockZ(), max.getBlockZ()) + 1;

        if(portal.isActive()){
            for(Player p : world.getPlayers()){
                Location loc = p.getLocation();
                if( minX <= loc.getBlockX() && loc.getBlockX() <= maxX &&
                    minY <= loc.getBlockY() && loc.getBlockY() <= maxY &&
                    minZ <= loc.getBlockZ() && loc.getBlockZ() <= maxZ){
                    Bukkit.getPluginManager().callEvent(new PortalEnterEvent(p, portal));
                }
            }

            for(double x=minX; x<=maxX; x+=0.5){
                for(double y=minY; y<=maxY; y+=0.5){
                    for(double z=minZ; z<=maxZ; z+=0.5){
                        world.spawnParticle(Particle.VILLAGER_HAPPY, new Location(world, x, y, z), 1);
                    }
                }
            }
        }
    }
}
