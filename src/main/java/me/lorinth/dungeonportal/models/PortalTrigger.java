package me.lorinth.dungeonportal.models;

import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;

import java.util.List;

public class PortalTrigger {

    private double chance;
    private List<String> messages;

    private final String relativePath = ".Trigger.";

    public PortalTrigger(FileConfiguration config, String path){
        path += relativePath;

        String chanceValue = config.getString(path + "Chance").replace('%', ' ').trim();
        chance = Double.parseDouble(chanceValue);
        messages = config.getStringList(path + "Messages");

        for(int i=0; i<messages.size(); i++){
            messages.set(i, ChatColor.translateAlternateColorCodes('&', messages.get(i)));
        }
    }

    public double getChance() {
        return chance;
    }

    public void setChance(double chance) {
        this.chance = chance;
    }

    public List<String> getMessages() {
        return messages;
    }

    public void setMessage(List<String> messages) {
        this.messages = messages;
    }
}
