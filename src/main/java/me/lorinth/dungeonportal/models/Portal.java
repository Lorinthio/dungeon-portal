package me.lorinth.dungeonportal.models;

import me.lorinth.dungeonportal.DungeonPortalPlugin;
import me.lorinth.dungeonportal.tasks.CheckPortalTask;
import me.lorinth.dungeonportal.tasks.PortalActivationTask;
import me.lorinth.dungeonportal.util.DungeonPortalOutputHandler;
import me.lorinth.utils.ConfigHelper;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

public class Portal {

    private String id;
    private Location minimum;
    private Location maximum;
    private List<PossibleDungeonResult> worldResultList;
    private Integer activationInterval;
    private PortalTrigger trigger;
    private PortalInstance currentInstance;
    private Random random = new Random();
    private boolean valid = true;
    private Long nextTrigger;

    public Portal(FileConfiguration config, String path, String id){
        this.id = id;
        load(config, path + "." + id);

        if(valid){
            startTasks();
        }
    }

    public String getId(){
        return id;
    }

    //Is the portal active?
    public boolean isActive(){
        return currentInstance != null;
    }

    //Is the portal active and ready to be claimed?
    public boolean isOpen(){
        return isActive() && currentInstance.getPartyLeaderName() == null;
    }

    public Location getMinimum() {
        return minimum;
    }

    public Location getMaximum() {
        return maximum;
    }

    public List<PossibleDungeonResult> getResultList() {
        return worldResultList;
    }

    //Called when trigger is satisfied
    public PortalInstance activate(){
        int randomIndex = random.nextInt(worldResultList.size());
        PossibleDungeonResult dungeonResult = worldResultList.get(randomIndex);
        PossibleBossResult bossResult = dungeonResult.getNextBossResult();

        for(String line : trigger.getMessages()){
            Bukkit.broadcastMessage(line);
        }

        currentInstance = new PortalInstance(bossResult, null, dungeonResult.getStartPosition());
        return currentInstance;
    }

    public PortalInstance start(Player leader){
        currentInstance.setPartyLeaderName(leader.getDisplayName());
        return currentInstance;
    }

    public void reset(){
        currentInstance = null;
    }

    public PortalTrigger getTrigger(){
        return trigger;
    }

    public PortalInstance getCurrentInstance() {
        return currentInstance;
    }

    public void updateNextTriggered(){
        nextTrigger = System.currentTimeMillis() + this.activationInterval * 1000;
    }

    public boolean isValid(){
        return valid;
    }

    private void load(FileConfiguration config, String path){
        trigger = new PortalTrigger(config, path);
        if(ConfigHelper.ConfigContainsPath(config, path + ".CurrentInstance") &&
            config.get(path + ".CurrentInstance") != null){
            currentInstance = new PortalInstance(config, path);
        }

        activationInterval = config.getInt(path + ".Interval");

        loadPosition(config, path);
        loadDungeonResults(config, path);

        DungeonPortalOutputHandler.PrintInfo("Loaded Portal, " + DungeonPortalOutputHandler.HIGHLIGHT + this.id);
    }

    private void loadPosition(FileConfiguration config, String path){
        path += ".Position";
        String worldName = config.getString(path + ".World");
        World world = Bukkit.getWorld(worldName);
        if(Bukkit.getWorld(worldName) == null){
            DungeonPortalOutputHandler.PrintInfo("Portal " +
                    DungeonPortalOutputHandler.HIGHLIGHT + id +
                    DungeonPortalOutputHandler.INFO + " in non-existent world " +
                    DungeonPortalOutputHandler.HIGHLIGHT + worldName +
                    DungeonPortalOutputHandler.INFO + ". Ignoring...");
            valid = false;
            return;
        }

        String minPath = path + ".Minimum.";
        minimum = new Location(world,
                config.getInt(minPath + "X"),
                config.getInt(minPath + "Y"),
                config.getInt(minPath + "Z"));

        String maxPath = path + ".Maximum.";
        maximum = new Location(world,
                config.getInt(maxPath + "X"),
                config.getInt(maxPath + "Y"),
                config.getInt(maxPath + "Z"));
    }

    private void loadDungeonResults(FileConfiguration config, String path){
        path += ".TargetWorlds";
        worldResultList = new ArrayList<>();

        for(String worldName : config.getConfigurationSection(path).getKeys(false)){
            worldResultList.add(new PossibleDungeonResult(config, path, worldName));
        }
    }

    public void save(FileConfiguration config, String path){
        path += "." + id;

        if(currentInstance != null){
            currentInstance.save(config, path);
        }
        else{
            config.set(path + ".CurrentInstance", null);
        }
        config.set(path + ".NextTrigger", nextTrigger);
    }

    private void startTasks(){
        long nextRun = activationInterval;
        if(nextTrigger != null){
            nextRun = (nextTrigger - System.currentTimeMillis()) / 1000;
            if(nextRun > activationInterval){
                nextRun = activationInterval;
                updateNextTriggered();
            }
        }
        else{
            updateNextTriggered();
        }
        new PortalActivationTask(this).runTaskTimer(DungeonPortalPlugin.getInstance(), nextRun * 20, activationInterval * 20);
        new CheckPortalTask(this).runTaskTimer(DungeonPortalPlugin.getInstance(), 10, 10);
    }
}
