package me.lorinth.dungeonportal.models;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.configuration.file.FileConfiguration;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class PossibleDungeonResult {

    private String worldName;
    private Location startPosition;
    private List<PossibleBossResult> bossResultList;
    private Random random = new Random();

    public PossibleDungeonResult(FileConfiguration config, String path, String worldName){
        path += "." + worldName;

        this.worldName = worldName;
        load(config, path);
    }

    public PossibleBossResult getNextBossResult(){
        int randomIndex = random.nextInt(bossResultList.size());
        return bossResultList.get(randomIndex);
    }

    public String getWorldName(){
        return worldName;
    }

    public Location getStartPosition(){
        return startPosition;
    }

    private void load(FileConfiguration config, String path){
        loadStartPosition(config, path);
        loadBossOptions(config, path);
    }

    private void loadStartPosition(FileConfiguration config, String path){
        String startPath = path + ".StartPosition";
        World world = Bukkit.getWorld(worldName);
        startPosition = new Location(world,
                config.getInt(startPath + ".X"),
                config.getInt(startPath + ".Y"),
                config.getInt(startPath + ".Z"),
                (float) config.getDouble(startPath + ".Yaw"),
                (float) config.getDouble(startPath + ".Pitch"));
    }

    private void loadBossOptions(FileConfiguration config, String path){
        bossResultList = new ArrayList<>();

        path = path + ".Bosses";
        World world = Bukkit.getWorld(worldName);
        for(String bossId : config.getConfigurationSection(path).getKeys(false)){
            String bossPath = path + "." + bossId + ".";
            Integer level = config.getInt(bossPath + "Level");
            Location spawnPosition = new Location(
                    world,
                    config.getInt(bossPath + "Position.X"),
                    config.getInt(bossPath + "Position.Y"),
                    config.getInt(bossPath + "Position.Z"),
                    (float) config.getDouble(bossPath + "Position.Yaw"),
                    (float) config.getDouble(bossPath + "Position.Pitch")
            );

            bossResultList.add(new PossibleBossResult(bossId, level, spawnPosition));
        }
    }

}
