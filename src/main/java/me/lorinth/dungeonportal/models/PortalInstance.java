package me.lorinth.dungeonportal.models;

import me.lorinth.dungeonportal.util.DungeonPortalOutputHandler;
import me.lorinth.utils.ConfigHelper;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.configuration.file.FileConfiguration;

public class PortalInstance {

    private PossibleBossResult bossResult;
    private String partyLeaderName;
    private Location startPosition;

    private final String relativePath = ".CurrentInstance.";

    public PortalInstance(PossibleBossResult bossResult, String partyLeaderName, Location startLocation){
        this.bossResult = bossResult;
        this.partyLeaderName = partyLeaderName;
        this.startPosition = startLocation;

        DungeonPortalOutputHandler.PrintInfo(this.toString());
    }

    public PortalInstance(FileConfiguration config, String path){
        load(config, path);
    }

    public Location getStartLocation(){
        return startPosition;
    }

    public String getPartyLeaderName(){
        return partyLeaderName;
    }

    public void setPartyLeaderName(String leaderName){
        partyLeaderName = leaderName;
    }

    public PossibleBossResult getBossResult(){
        return bossResult;
    }

    public void save(FileConfiguration config, String path){
        path += relativePath;

        config.set(path + "Boss.Id", bossResult.getBossId());
        config.set(path + "Boss.Level", bossResult.getLevel());
        config.set(path + "Boss.SpawnLocation.World", bossResult.getSpawnLocation().getWorld().getName());
        config.set(path + "Boss.SpawnLocation.X", bossResult.getSpawnLocation().getBlockX());
        config.set(path + "Boss.SpawnLocation.Y", bossResult.getSpawnLocation().getBlockY());
        config.set(path + "Boss.SpawnLocation.Z", bossResult.getSpawnLocation().getBlockZ());
        config.set(path + "Boss.SpawnLocation.Yaw", bossResult.getSpawnLocation().getYaw());
        config.set(path + "Boss.SpawnLocation.Pitch", bossResult.getSpawnLocation().getPitch());

        config.set(path + "PartyLeaderName", partyLeaderName != null ? partyLeaderName : "");
        config.set(path + "StartLocation.World", startPosition.getWorld().getName());
        config.set(path + "StartLocation.X", startPosition.getBlockX());
        config.set(path + "StartLocation.Y", startPosition.getBlockY());
        config.set(path + "StartLocation.Z", startPosition.getBlockZ());
        config.set(path + "StartLocation.Yaw", startPosition.getYaw());
        config.set(path + "StartLocation.Pitch", startPosition.getPitch());
    }

    private void load(FileConfiguration config, String path){
        path += relativePath;

        String worldName;
        int x, y, z;
        float yaw, pitch;

        bossResult = null;
        if(ConfigHelper.ConfigContainsPath(config, path + "Boss.Id")){
            String bossId = config.getString(path + "Boss.Id");
            Integer level = config.getInt(path + "Boss.Level");

            worldName = config.getString(path + "Boss.SpawnLocation.World");
            x = config.getInt(path + "Boss.SpawnLocation.X");
            y = config.getInt(path + "Boss.SpawnLocation.Y");
            z = config.getInt(path + "Boss.SpawnLocation.Z");
            yaw = (float) config.getDouble(path + "Boss.SpawnLocation.Yaw");
            pitch = (float) config.getDouble(path + "Boss.SpawnLocation.Pitch");
            Location spawnLocation = new Location(Bukkit.getWorld(worldName), x, y, z, yaw, pitch);

            bossResult = new PossibleBossResult(bossId, level, spawnLocation);
        }
        partyLeaderName = config.getString(path + "PartyLeaderName");

        worldName = config.getString(path + "StartLocation.World");
        x = config.getInt(path + "StartLocation.X");
        y = config.getInt(path + "StartLocation.Y");
        z = config.getInt(path + "StartLocation.Z");
        yaw = (float) config.getDouble(path + "StartLocation.Yaw");
        pitch = (float) config.getDouble(path + "StartLocation.Pitch");

        startPosition = new Location(Bukkit.getWorld(worldName), x, y, z, yaw, pitch);
    }

    @Override
    public String toString() {
        return "PortalInstance{" +
                "bossId='" + bossResult + '\'' +
                ", partyLeaderName='" + partyLeaderName + '\'' +
                ", startPosition=" + startPosition +
                '}';
    }
}
