package me.lorinth.dungeonportal.models;

import org.bukkit.Location;

public class PossibleBossResult {

    private String bossId;
    private Integer level;
    private Location spawnLocation;

    public PossibleBossResult(String bossId, Integer level, Location spawnLocation){
        this.bossId = bossId;
        this.level = level;
        this.spawnLocation = spawnLocation;
    }

    public String getBossId(){
        return bossId;
    }

    public Integer getLevel(){
        return level;
    }

    public Location getSpawnLocation(){
        return spawnLocation;
    }

    @Override
    public String toString() {
        return "PossibleBossResult{" +
                "bossId='" + bossId + '\'' +
                ", level=" + level +
                ", spawnLocation=" + spawnLocation +
                '}';
    }
}
