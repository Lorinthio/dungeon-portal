package me.lorinth.dungeonportal;

import me.lorinth.dungeonportal.boss.BossSpawnHandler;
import me.lorinth.dungeonportal.boss.IBossHandler;
import me.lorinth.dungeonportal.boss.MythicMobsSpawnHandler;
import me.lorinth.dungeonportal.commands.PortalCommand;
import me.lorinth.dungeonportal.listeners.PortalListener;
import me.lorinth.dungeonportal.managers.PortalManager;
import me.lorinth.dungeonportal.messages.MessageHandler;
import me.lorinth.dungeonportal.models.Portal;
import me.lorinth.dungeonportal.util.DungeonPortalOutputHandler;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;

public class DungeonPortalPlugin extends JavaPlugin {

    private static PortalManager portalManager;
    private static DungeonPortalPlugin instance;
    private static IBossHandler bossHandler;

    private final String portalsPath = "Portals";

    @Override
    public void onEnable() {
        instance = this;
        setup();
    }

    @Override
    public void onDisable() {
        save();
    }

    public static DungeonPortalPlugin getInstance(){
        return instance;
    }

    public static PortalManager getPortalManager(){
        return portalManager;
    }

    public static IBossHandler getBossHandler(){
        return bossHandler;
    }

    private void setup(){
        new DungeonPortalOutputHandler();
        checkForBossHandlers();

        loadConfig();
        setupListeners();
        setupCommands();
    }

    private void loadConfig(){
        saveDefaultConfig();

        File configFile = new File(getDataFolder(), "config.yml");
        FileConfiguration config = YamlConfiguration.loadConfiguration(configFile);

        //Load Messages
        new MessageHandler(config);
        loadPortals(config);
    }

    private void loadPortals(FileConfiguration config){
        portalManager = new PortalManager();
        for(String key : config.getConfigurationSection(portalsPath).getKeys(false)){
            portalManager.addPortal(new Portal(config, portalsPath, key));
        }
    }

    private void setupListeners(){
        Bukkit.getPluginManager().registerEvents(new PortalListener(), this);
    }

    private void setupCommands(){
        this.getCommand("portal").setExecutor(new PortalCommand());
    }

    private void save(){
        File configFile = new File(getDataFolder(), "config.yml");
        FileConfiguration config = YamlConfiguration.loadConfiguration(configFile);

        savePortals(config);
        try{
            config.save(configFile);
        }
        catch(Exception e){
            e.printStackTrace();
        }
    }

    private void savePortals(FileConfiguration config){
        for(Portal portal : portalManager.getAllPortals()){
            portal.save(config, portalsPath);
        }
    }

    private void checkForBossHandlers(){
        checkForBossPluginHandler();
        checkForMythicMobsSpawnHandler();
    }

    private void checkForBossPluginHandler() {
        try{
            Plugin bossPlugin = Bukkit.getPluginManager().getPlugin("Boss");
            if (bossPlugin != null && bossPlugin.isEnabled()){
                bossHandler = new BossSpawnHandler();
                DungeonPortalOutputHandler.PrintInfo("Detected Boss plugin for Boss spawning!");
            }
        }
        catch(Exception e){
            DungeonPortalOutputHandler.PrintException("Failed to find Boss plugin", e);
            bossHandler = null;
        }
    }

    private void checkForMythicMobsSpawnHandler() {
        try{
            Plugin mythicMobsPlugin = Bukkit.getPluginManager().getPlugin("MythicMobs");
            if (mythicMobsPlugin != null && mythicMobsPlugin.isEnabled()){
                bossHandler = new MythicMobsSpawnHandler();
                DungeonPortalOutputHandler.PrintInfo("Detected MythicMobs plugin for Boss spawning!");
            }
        }
        catch(Exception e){
            DungeonPortalOutputHandler.PrintException("Failed to find MythicMobs plugin", e);
            bossHandler = null;
        }
    }
}
